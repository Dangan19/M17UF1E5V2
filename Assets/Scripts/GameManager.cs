using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] public int playerHealth;
    [SerializeField] private Text textHealth;
    [SerializeField] private Text textMeters;
    [SerializeField] private GameObject gameOverCanvas;
    [SerializeField] private GameObject player;
    [SerializeField] private Button playAgain;

    public float meters;
    private static GameManager _instance;

        public static GameManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    Debug.LogError("Game Manager is NULL!");
                }

                return _instance;
            }


        }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {

            Destroy(this.gameObject);
        }

        _instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        playAgain.onClick.AddListener(playAgainButton);
    }

    // Update is called once per frame
    void Update()
    {
        if (playerHealth <= 0)
        {
            gameOver();

        }

        textHealth.text = "HEALTH: " + playerHealth;
        textMeters.text = "METERS: " + (float)System.Math.Round(meters * 100f) / 100f;

    }

    void gameOver() {
        gameOverCanvas.SetActive(true);
        Destroy(player);
        
    }

    void playAgainButton() {
        SceneManager.LoadScene("Game");
    }


}
