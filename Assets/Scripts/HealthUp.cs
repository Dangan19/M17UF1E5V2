using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthUp : MonoBehaviour
{
    [SerializeField] private int healthRegen;  


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player")) {
            Destroy(gameObject);

            if (GameManager.Instance.playerHealth < 3) {

                GameManager.Instance.playerHealth+=healthRegen;
            }
        }
    }
}
