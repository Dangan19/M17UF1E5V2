using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataEnemy : MonoBehaviour
{
    private GameObject dp;
    public Transform originPoint;
    private Vector2 dir = new Vector2(-1, 0);
    public float range = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        dp = GameObject.FindGameObjectWithTag("Player");


    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(originPoint.position, Vector2.left);
        //Debug.DrawRay(transform.position, Vector2.left);

        if (hit == true) {

           // Debug.Log("hit");
           // Debug.Log(hit.collider);

            if (hit.collider.CompareTag("Player")) {
                transform.position = Vector3.MoveTowards(transform.position, dp.transform.position, 0.01f );
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("collision ");
        if (collision.gameObject.tag == ("Player")) {
           // Debug.Log("dintre");

            Destroy(this);
            GameManager.Instance.playerHealth = 0;
        }
    }
}
