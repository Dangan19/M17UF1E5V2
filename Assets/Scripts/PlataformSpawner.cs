using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformSpawner : MonoBehaviour
{
    [SerializeField] private float width;
    [SerializeField] private float minWidth;
    [SerializeField] private float maxWidth;
    [SerializeField] private float spacing;
    [SerializeField] private GameObject platform;
    private int y = 0;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Spawner", 0.5f, 1);
    }

    void Spawner()
    {
        width = Random.Range(minWidth, maxWidth);
                 
        Instantiate(platform, new Vector3(width, y * spacing, 0), Quaternion.identity);
        y++;
        

    }


}
