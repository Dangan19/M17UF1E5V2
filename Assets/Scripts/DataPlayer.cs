using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataPlayer : MonoBehaviour
{
    [SerializeField] public float speed;
    [SerializeField] private float jumpStrengh;
    [SerializeField] private SpriteRenderer mySpriteRenderer;

    private bool onGround;
    private Rigidbody2D rb;
    private Collider2D gc;
    private Vector3 lastPosition;
    private float odometerDistance;
    private bool facingRight;


    // Start is called before the first frame update
    void Start()
    {
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();

        
    }

    private void Update()
    {


    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Space) && onGround)
        {
            Jump();

        }


        //movment
        float h = Input.GetAxisRaw("Horizontal");


        if (h > 0 && facingRight)
        {
            Flip();

        } else if (h < 0 && !facingRight)
        {
            Flip();
        }

    


        Vector2 movement = new Vector2(h, 0);
        rb.AddForce(movement * speed * Time.deltaTime);
    }

    private void Jump()
    {    
            //Debug.Log("Dejump");
            rb.AddForce(transform.up * jumpStrengh * Time.deltaTime);
            onGround = false;
        
    }

    public void CollisionDetected(ColliderControler childScript)
    {
            onGround = true;       
    }


    private void OnTriggerEnter2D(Collider2D collider2D)
    {
        if (collider2D.tag == "MainCamera")
        {
            lastPosition = transform.position;
            GameManager.Instance.playerHealth--;
            

            if (GameManager.Instance.playerHealth > 0)
            {
                lastPosition.y = lastPosition.y + 3;
                this.transform.position = lastPosition;
            }
            
        }
    }


    void Flip()
    {
        facingRight = !facingRight;
        transform.Rotate(new Vector3(0, 180, 0));
    }

}
