using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public Transform player;
    private Vector3 startPosition;
    private float max;
    private float odometerDistance;

    // Start is called before the first frame update
    void Start()
    {
         startPosition = gameObject.transform.position;
        transform.position = new Vector3(startPosition.x + 0, player.position.y + 3, -10);
        max = player.position.y;
        odometerDistance = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (max <= player.position.y) {
            transform.position = new Vector3(startPosition.x + 0, player.position.y + 3, -10);
            max = player.position.y;
            odometerDistance =+ max;
            GameManager.Instance.meters = odometerDistance;
        }
    }

}
